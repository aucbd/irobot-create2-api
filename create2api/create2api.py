import struct
from math import copysign
from typing import Tuple

import serial

from .config import config


def two_complement(n: int) -> int:
    if n < 0:
        return 2 ** 16 - abs(n)
    elif n == 0:
        return 0
    else:
        return n


def split_byte(n: int) -> Tuple[int, int]:
    return n >> 8, n % (2 ** 8)


class SerialConnection:
    def __init__(self, port: str, baud: int):
        self.port = port
        self.baud = baud

        self.connection: serial.Serial = serial.Serial()
        self.connection.port = port
        self.connection.baudrate = baud

        if self.connection.isOpen():
            self.connection.close()

        self.connection.open()

    def send(self, opcode: int, *data: int) -> int:
        bytes_tuple: tuple = (opcode,) + data

        return self.connection.write(struct.pack('B' * len(bytes_tuple), *bytes_tuple))

    def close(self):
        self.connection.close()


class Create2API:
    def __init__(self, port: str = "/dev/ttyUSB0", baud: int = 115200):
        self.serial: SerialConnection = SerialConnection(port, baud)
        self.config: dict = config

    def connect(self, port: str, baud: int):
        self.serial = SerialConnection(port, baud)

    def disconnect(self):
        self.stop()
        self.serial.close()

    def start(self) -> int:
        return self.serial.send(128)

    def reset(self) -> int:
        return self.serial.send(7)

    def stop(self) -> int:
        return self.serial.send(173)

    def baud(self, baud: int) -> int:
        return self.serial.send(129, self.config["baud_rates"].get(baud, self.serial.baud))

    def safe(self) -> int:
        return self.serial.send(131)

    def full(self) -> int:
        return self.serial.send(132)

    def power(self) -> int:
        return self.serial.send(133)

    def drive(self, velocity: int, radius: int = 0) -> int:
        velocity = int(copysign(500, velocity)) if abs(velocity) > 500 else velocity
        radius = int(copysign(500, radius)) if abs(radius) > 500 else radius

        velocity = two_complement(velocity)
        if radius != 0:
            radius = two_complement(radius)
        else:
            radius = 32768

        return self.serial.send(137, *split_byte(velocity), *split_byte(radius))

    def drive_direct(self, velocity_right: int, velocity_left: int) -> int:
        velocity_right = int(copysign(500, velocity_right)) if abs(velocity_right) > 500 else velocity_right
        velocity_left = int(copysign(500, velocity_left)) if abs(velocity_left) > 500 else velocity_left

        velocity_right = two_complement(velocity_right)
        velocity_left = two_complement(velocity_left)

        return self.serial.send(145, *split_byte(velocity_right), *split_byte(velocity_left))

    def drive_pwm(self, pwm_right: int, pwm_left: int) -> int:
        pwm_right = int(copysign(255, pwm_right)) if abs(pwm_right) > 255 else pwm_right
        pwm_left = int(copysign(255, pwm_left)) if abs(pwm_left) > 255 else pwm_left

        pwm_right = two_complement(pwm_right)
        pwm_left = two_complement(pwm_left)

        return self.serial.send(146, *split_byte(pwm_right), *split_byte(pwm_left))

    def drive_stop(self) -> int:
        return self.serial.send(137, *split_byte(0), *split_byte(0))

    def song(self, song_number, *notes: Tuple[str, int]) -> int:
        notes = notes[:16]

        notes_int: tuple = tuple(e for note in notes for e in (self.config["midi"].get(note[0], 0), note[1]))

        return self.serial.send(140, song_number, len(notes), *notes_int)

    def play(self, song_number: int) -> int:
        return self.serial.send(141, song_number)
